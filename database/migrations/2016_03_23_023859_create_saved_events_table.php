<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saved_events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->dateTime('remind_on');
            $table->timestamps();
        });

        Schema::table('saved_events', function (Blueprint $table){
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unique(['event_id', 'user_id'], 'user_event_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saved_events', function ($table) {
            $table->dropForeign(['event_id']);
            $table->dropForeign(['user_id']);
            $table->dropUnique('user_event_index');
        });
        
        Schema::drop('saved_events');
    }
}
