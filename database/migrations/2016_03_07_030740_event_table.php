<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('category')->unsigned();
            $table->datetime('start_on');
            $table->integer('price');
            $table->integer('created_by')->unsigned();
            $table->string('organizer');
            $table->text('address');
            $table->boolean('is_approved');
            $table->boolean('is_done');
            $table->text('image_url');
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('events', function (Blueprint $table){
            $table->foreign('created_by')->references('id')->on('users');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function ($table) {
            $table->dropForeign(['created_by']);
        });

        Schema::drop('events');
    }
}
