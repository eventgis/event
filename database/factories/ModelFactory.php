<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->freeEmail,
        'username' => $faker->userName,
        'password' => "12345",
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Event::class, function (Faker\Generator $faker){

    $image = ['images/music.jpg', 'images/festival.jpg', 'images/arts.jpg'  ];
	return [
		'name' => 'Event '.$faker->catchPhrase,
		'description' => $faker->realText,
        'category' => rand(1,11),
        'start_on' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+1 years'),
        'price' => $faker->randomNumber(3),
        'created_by' => 1,
        'organizer' => $faker->company,
        'address' => $faker->address,
        'is_approved' => $faker->boolean($chanceOfGettingTrue = 88),
        'is_done' => $faker->boolean($chanceOfGettingTrue = 68),
        'image_url' => $image[rand(0,2)],
        'latitude' => rand(6200, 6500)/-1000,
        'longitude' => rand(106400, 106900)/1000
	];
});

