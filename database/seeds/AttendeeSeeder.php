<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Event;
use App\Models\User;
use App\Models\Attendee;

class AttendeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
    	$events = Event::lists('id')->all();
    	$users = User::lists('id')->all();
    	$certainty = ['uncertain', 'certain'];

        foreach(range(1, User::all()->count()) as $index){
            $event = Event::find($faker->randomElement($events));
            $attendee = Attendee::create([
                'event_id' => $event->id,
                'user_id' => $faker->randomElement($users),
            ]);
        }
    }
}
