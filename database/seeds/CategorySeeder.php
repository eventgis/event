<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = 
        [
        "Music", 
        "Sports", 
        "Food & Drink", 
        "Arts", 
        "Parties", 
        "Festival", 
        "Workshop", 
        "Seminar", 
        "Conference", 
        "Gathering & Networking", 
        "Other"];

        for ($i=0; $i < count($categories); $i++) { 
        	$category = ['category_name' => $categories[$i]];
        	Category::create($category);
        }
    }
}
