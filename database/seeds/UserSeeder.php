<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $elgisuser = 
        ['name' => 'ELGIS organizer', 
        'username' => 'elgisadmin', 
        'email' => 'hanggi_anggono@yahoo.com', 
        'role' => 'admin', 
        'password' => 'hanggian',
        'confirmed' => 1];

        User::create($elgisuser);

        //factory(App\Models\User::class, 30)->create();
    }
}
