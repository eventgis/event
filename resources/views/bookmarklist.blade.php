<!-- BOOKMARK-->
@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="{{url('new')}}">All Event</a></li>
						    	@if (Auth::check())
						    	<li><a href="{{url('add')}}">Add Event</a></li>
						    	<li class="current"><a href="{{url('bookmarklist')}}">Bookmark</a></li>
						    	@else
						    	<li><a href="{{url('login')}}">Add Event</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li><a href="{{url('maps')}}">Maps</a></li>
						   
								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')

<div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="row team_box">
				<h3 class="m_2"><center>{{$us->name}} Bookmark List</center></h3>

				@foreach($bookmark as $bm)
					@if ($bm->user_id == $us->id)

				<div class="col-md-3 team1">
				  <a class="popup-with-zoom-anim" href="#small-dialog3"><img src="{{URL::asset($bm->event->image_url) }}" width="100%" height="270px" title="{{$bm->event->name}}" alt=""/></a>
				    <div id="small-dialog3" class="mfp-hide">
					   <div class="pop_up2">
					   	 <h2>{{$bm->event->name}}</h2>
						 <p>{{$bm->event->description}}</p><br>
					   <div class="btn_form">
					   <a href="bookmarklist/{{ $bm -> id }}/delete" onclick="return confirm('Are you sure you want to delete from Bookmark ?')">Delete From Bookmark</a>
					   </div>
					   </div>
					</div>
					<h4 class="m_5"><a href="detail/{{$bm->event->id}}"><center>{{$bm->event->name}}</center></a></h4>
				</div>
				@endif
				@endforeach
				
			
			</div>
		 </div>
	   </div>
	  </div>

		@stop