<style>
	#map-canvas{
		width: 1170px;
		height: 500px;
	}
</style>

<script type="text/javascript">
  function initialize() {
    var mapOptions = {
      center: new google.maps.LatLng(-6.2327029, 106.8606139),
      zoom: 8
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"),
        mapOptions);
  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places"
type="text/javascript"></script>
@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="{{url('new')}}">New</a></li>
						    	<li><a href="{{url('category')}}">Category</a></li>
						    	@if (Auth::check())
						    	<li><a href="{{url('add')}}">Add Event</a></li>
						    	@else
						    	<li><a href="{{url('login')}}">Add Event</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li class="current"><a href="{{url('maps')}}">Maps</a></li>
								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')
<div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="row shop_box-top">
				
				{{$ev->count()}}
              			<div id="map-canvas" class="map">
              			</div>
              			
			
			</div>
		 </div>
	   </div>
	  </div>  
	@foreach ($ev as $ev)
<script>

var db_markers = {{$ev}};

var marker, x, infowindow = new google.maps.InfoWindow();

for (x = 0; x < db_markers.length; x++) {
        var id = db_markers[x];
        var location = new google.maps.LatLng(id.latitude,id.longitude);
        var marker = new google.maps.Marker({
        position: location,
        title: id.name,
        map: map
        });

    google.maps.event.addListener(marker, 'click', (function(marker, x) {
        return function() {
            infowindow.setContent("<b>Title: " + db_markers[x].title);
            infowindow.open(map, marker);
        }
    })(marker, x));
}

</script>
@endforeach
		@stop
