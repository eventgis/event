<style>
	#map-canvas{
		width: 1170px;
		height: 500px;
	}
</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places"
type="text/javascript"></script>
@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="{{url('new')}}">All Event</a></li>
						    	@if (Auth::check())
						    	<li><a href="{{url('add')}}">Add Event</a></li>
						    	<li><a href="{{url('bookmarklist')}}">Bookmark</a></li>
						    	@else
						    	<li><a href="{{url('login')}}">Add Event</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li class="current"><a href="{{url('maps')}}">Maps</a></li>
								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')

<div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="row shop_box-top">
				
				<label>Search Location</label>
                		<input type="text" name="lokasi" class="form-control" id="searchmap"/>
                		
              			<div id="map-canvas" class="map">
              			</div>
              			
			
			</div>
		 </div>
	   </div>
	  </div>
	  


<script>

var map= new google.maps.Map(document.getElementById('map-canvas'),{
	center:{
		lat:-6.2293867,
		lng:106.6894296
	},
	zoom:10
});
    </script>
</script>
@foreach ($ev as $ev)
<script>
var lat = {{$ev->latitude}};
var lng = {{$ev->longitude}};
var judul = '{{$ev->name}}' + '<br>'+'{{$ev->start_on}}'+'<br>'+ '<br>'+'<a href="{{'detail/'.$ev->id}}">Detail Event</a>';

var marker = new google.maps.Marker({
	position: {
		lat:lat,
		lng:lng
	},
	map: map,
	title : '{{$ev->name}}',
});

attachDetail(marker, judul);

function attachDetail(marker, judul) {
        var infowindow = new google.maps.InfoWindow({
          content: judul
        });

        marker.addListener('click', function() {
          infowindow.open(marker.get('map'), marker);
        });
      }

</script>
@endforeach

<script>
var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

google.maps.event.addListener(searchBox, 'places_changed', function(){
	var places = searchBox.getPlaces();
	var bounds = new google.maps.LatLngBounds();
	var i, place;

	for (i=0; place=places[i]; i++){
		bounds.extend(place.geometry.location);
	//	marker.setPosition(place.geometry.location);//set marker position new

	}

	map.fitBounds(bounds);
	map.setZoom(15);

});

</script>
		@stop