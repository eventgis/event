@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="{{url('new')}}">New</a></li>
						    	<li class="current"><a href="{{url('category')}}">Category</a></li>
						    	@if (Auth::check())
						    	<li><a href="{{url('add')}}">Add Event</a></li>
						    	@else
						    	<li><a href="{{url('login')}}">Add Event</a></li>
						    	@endif 
						    	<li><a href="{{url('maps')}}">Maps</a></li>
								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')


<div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="row ex_box">
				<h3 class="m_2">Category Event</h3>
				<div class="col-md-4 team1">
				<div class="img_section magnifier2">
				  <a class="fancybox" href="{{('new')}}" data-fancybox-group="gallery" title="Music"><img src="images/music.jpg" width="100%" height="270px" alt=""><span> </span>
					<div class="img_section_txt">
						Music
					</div>
				</a></div>
				</div>
				<div class="col-md-4 team1">
				<div class="img_section magnifier2">
				  <a class="fancybox" href="{{('new')}}" data-fancybox-group="gallery" title="Food & Drink"><img src="images/food.jpg"  width="100%" height="270px" alt=""><span> </span>
					<div class="img_section_txt">
						Food & Drink
					</div>
				</a></div>
				</div>
				<div class="col-md-4 team1">
				<div class="img_section magnifier2">
				  <a class="fancybox" href="{{('new')}}" data-fancybox-group="gallery" title="Arts"><img src="images/arts.jpg"  width="100%" height="270px" alt=""><span> </span>
					<div class="img_section_txt">
						Arts
					</div>
				</a></div>
				</div>
		    </div>
		    <div class="row ex_box">
				<div class="col-md-4 team1">
				<div class="img_section magnifier2">
				  <a class="fancybox" href="{{('new')}}" data-fancybox-group="gallery" title="Parties"><img src="images/parties.jpg"  width="100%" height="270px" alt=""><span> </span>
					<div class="img_section_txt">
						Parties
					</div>
				</a></div>
				</div>
				<div class="col-md-4 team1">
				<div class="img_section magnifier2">
				  <a class="fancybox" href="{{('new')}}" data-fancybox-group="gallery" title="Sports"><img src="images/sports.png"  width="100%" height="270px" alt=""><span> </span>
					<div class="img_section_txt">
						Sports
					</div>
				</a></div>
				</div>
		    <div class="row ex1_box">
			   <div class="col-md-4 team1">
				<div class="img_section magnifier2">
				  <a class="fancybox" href="{{('new')}}" data-fancybox-group="gallery" title="Festival"><img src="images/festival.jpg"  width="100%" height="270px" alt=""><span> </span>
					<div class="img_section_txt">
						Festival
					</div>
				</a></div>
				</div>
				
		    </div>
		 </div>
	   </div>
	  </div>

		@stop