<h1>ELGIS account activation</h1>
 
<p>
	Thank you for registering on ELGIS, before you can proceed with your account please click on the following link to activate you account. <br><br>
	<a href="{{route('activate', ['confirmation_code' => $user['confirmation_code']])}}">Activate</a>

	<br><br>
	You can also copy paste this link into you browser address bar
	{{route('activate', ['confirmation_code' => $user['confirmation_code']])}}
</p>