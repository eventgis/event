@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="{{url('new')}}">All Event</a></li>
						    	@if (Auth::check())
						    	<li class="current"><a href="{{url('add')}}">Add Event</a></li>
						    	<li><a href="{{url('bookmarklist')}}">Bookmark</a></li>
						    	@else
						    	<li class="current"><a href="{{url('login')}}">Add Event</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li><a href="{{url('maps')}}">Maps</a></li>
								<li><a href="{{url('about')}}">About</a></li>							
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')

 <div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="col-md-6">
				 <div class="login-page">
					<h4 class="title">Create an Account for Add Event</h4>
					<p aligne="justify">ELGIS is an application finding event with a Geographic Information System Technology. Elgis display information and location from various events category such as concert, celebrations, festival and etc. Information included name, date, description and location of event.</p>

					<div class="button1">
					   <a href="{{url('register')}}"><input type="submit" name="Submit" value="Create an Account"></a>
					 </div>
					 <div class="clear"></div>
				  </div>
				</div>
				<div class="col-md-6">
				 <div class="login-title">
	           		<h4 class="title">Login Member</h4>
					<div id="loginbox" class="loginbox">
						<form action="login" method="POST" name="login" id="login-form">
						  <fieldset class="input">
						    <p id="login-form-username">
						      <label for="modlgn_username">Email</label>
						      <input type="hidden" name="_token" value="{{ csrf_token() }}">
						      <input id="modlgn_username" type="text" name="email" class="inputbox" size="18" autocomplete="off">
						    </p>
						    <p id="login-form-password">
						      <label for="modlgn_passwd">Password</label>
						      <input id="modlgn_passwd" type="password" name="password" class="inputbox" size="18" autocomplete="off">
						    </p>
						    <div class="remember">
							    
							    <input type="submit" name="Submit" class="button" value="Login"><div class="clear"></div>
							 	
							 </div>
						  </fieldset>
						 </form>
					</div>
			      </div>
				 <div class="clear"></div>
			  </div>
			</div>
		  </div>
	  </div>

		@stop