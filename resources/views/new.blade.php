@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt=""/></a>
						    <ul class="nav" id="nav">
						    	<li class="current"><a href="{{url('new')}}">All Event</a></li>
						    	@if (Auth::check())
                				<li><a href="{{url('add')}}">Add Event</a></li>
                				<li><a href="{{url('bookmarklist')}}">Bookmark</a></li>
              						
						    	@else
						    	<li><a href="{{url('login')}}">Add Event</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li><a href="{{url('maps')}}">Maps</a></li>

								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')

<div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="row shop_box-top">
<div class="form-group">
<label>Search by Category</label>
 <form method="get" action="{{ action('newController@index') }}">
	                		<select onchange="this.form.submit()" name="category" class="selectpicker form-control">
	                			
	                			<option value="0"  @if($kat == 0 ) selected @endif>All Events</option>
	                			@foreach($category as $cat)
								<option value="{{$cat->id}}" @if($kat == 1 ) selected @endif>{{$cat->category_name}}</option>
								@endforeach
							</select>
						 </form>
</div>
<br><br>
				@foreach ($event as $ev)
					@if ($ev->is_approved == '1')
				<div class="col-md-3 shop_box"><a href="{{'detail/'.$ev->id}}">
					
					<img src="{{URL::asset($ev->image_url) }}" width="100%" height="270px" alt=""/>
					<!--<span class="new-box">
						<span class="new-label">{{$ev->category}}</span>
					</span>-->
					<span class="sale-box">
						
						<span class="sale-label">{{$ev->categori->category_name}}</span>
						
					</span>
					<div class="shop_desc">
						<h3><a href="{{'detail/'.$ev->id}}">{{$ev->name}}</a></h3>
						<p>{{$ev->start_on}}</p>
						<!--<span class="reducedfrom">Buy</span>-->
						@if ($ev->price == 0)
						<span class="actual">Free</span>
						@else
						<span class="actual">Rp. {{$ev->price}}</span>
						@endif
						<br>
						<ul class="buttons">
							<li class="cart"><a href="#">Add To Bookmark</a></li>
							<li class="shop_btn"><a href="{{'detail/'.$ev->id}}">Read More</a></li>
							<div class="clear"> </div>

					    </ul>
				    </div>
				</a>
				<br>
				<br>
			</div>
			@endif
				@endforeach
			</div>
			{{ $event->render() }} 
		 </div>
	   </div>
	  </div>

		@stop