<style>
	#map, #pano {
		float: left;
		height: 500px;
		width: 50%;
	}
</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places"
type="text/javascript"></script>
@extends('layout')
@section('menu')
<div class="menu">
	<a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
	<ul class="nav" id="nav">
		<li class="current"><a href="{{url('new')}}">All Event</a></li>

		@if (Auth::check())
		<li><a href="{{url('add')}}">Add Event</a></li>
		<li><a href="{{url('bookmarklist')}}">Bookmark</a></li>
		@else
		<li><a href="{{url('login')}}">Add Event</a></li>
		@endif
		<!--<li><a href="experiance.html">Experience</a></li>-->
		<li><a href="{{url('maps')}}">Maps</a></li>
		<li><a href="{{url('about')}}">About</a></li>								
		<div class="clear"></div>
	</ul>
	<script type="text/javascript" src="js/responsive-nav.js"></script>
</div>							

@stop

@section('content')
<div class="main">
	<div class="shop_top">
		<div class="container">
			<div class="row">
				<div class="col-md-9 single_left">
					<div class="single_image">


						<img class="etalage_source_image" src="{{URL::asset($event->image_url) }}" width="100%" height="300px"/>

					</div>
					<!-- end product_slider -->
					<div class="single_right">
						<h3>{{$event->name}}</h3>
						<p class="m_10">{{$event->description}}<p>
							<!--<ul class="options">-->
							<h4 class="m_12">Date</h4>
							<p class="m_10">{{$event->start_on}}</p>

							<h4 class="m_12">Price</h4>

							@if ($event->price == 0)
							<p class="m_10">Free</p>
							@else
							<p class="m_10">Rp. {{$event->price}}</p>
							@endif


							<!--
							<li><a href="#">151</a></li>
							<li><a href="#">148</a></li>
							<li><a href="#">156</a></li>
							<li><a href="#">145</a></li>
							<li><a href="#">162(w)</a></li>
							<li><a href="#">163</a></li>
						</ul>
			        	<ul class="product-colors">
							<h3>available Colors</h3>
							<li><a class="color1" href="#"><span> </span></a></li>
							<li><a class="color2" href="#"><span> </span></a></li>
							<li><a class="color3" href="#"><span> </span></a></li>
							<li><a class="color4" href="#"><span> </span></a></li>
							<li><a class="color5" href="#"><span> </span></a></li>
							<li><a class="color6" href="#"><span> </span></a></li>
							<div class="clear"> </div>
						</ul>-->
						@if (Auth::check())
							@if ($attend->count() == 0)
						<div class="btn_form">
							<form action="attend" method="post">
								<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
								<input type="hidden" name="event_id" value="{{$event->id}}">
								<input type="hidden" name="user_id" value="{{$us->id}}">
								<input type="submit" value="Attend" title="" onclick="return confirm('Are you sure you want to attend this event ?')">

							</form>
						</div>
							@else
							<div class="btn_form"><form action="{{url('attendlist')}}">
					   		<a href="{{url('attendlist')}}"><input type="submit" name="Submit" value="Cancel Attend"></a>
					 		</form></div>
							@endif
							@if ($bookmark->count() == 0)
						<div class="btn_form">
							<form action="bookmark" method="post">
								<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
								<input type="hidden" name="event_id" value="{{$event->id}}">
								<input type="hidden" name="user_id" value="{{$us->id}}">

								<input type="submit" value="Add to Bookmark" title="">

							</form>
						</div>
							@else
							<div class="btn_form"><form action="{{url('bookmarklist')}}">
					   		<input type="submit" name="Submit" value="Delete From Bookmark">
					 		</form></div>
							@endif
						@endif


						<h4>{{$totalattendee->count()}} People Attend</h4>
						
						<div class="social_buttons">
							<button type="button" class="btn1 btn1-default1 btn1-twitter" onclick="">
								<i class="icon-twitter"></i> Tweet
							</button>
							<button type="button" class="btn1 btn1-default1 btn1-facebook" onclick="">
								<i class="icon-facebook"></i> Share
							</button>
							<button type="button" class="btn1 btn1-default1 btn1-google" onclick="">
								<i class="icon-google"></i> Google+
							</button>
							<button type="button" class="btn1 btn1-default1 btn1-pinterest" onclick="">
								<i class="icon-pinterest"></i> Pinterest
							</button>
						</div>
					</div>
					<div class="clear"> </div>
				</div>
			<!--<div class="col-md-3">
			  <div class="box-info-product">
				<p class="price2">$130.25</p>
				       <ul class="prosuct-qty">
							<span>Quantity:</span>
							<select>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
								<option>6</option>
							</select>
						</ul>
						<button type="submit" name="Submit" class="exclusive">
						   <span>Add to cart</span>
						</button>
			   </div>
			</div>-->
		</div>		
		<br>
		<div class="desc">
			<br><br>
			<h4>MAPS</h4>
			<div id="map"></div>
			<div id="pano"></div>
			<a href="#getdirection" id="getdirection">Click For Direction</a>
		</div>
		<div>&nbsp;</div>
		<div class="row">

			<h4 class="m_11">Recommendations Event</h4>

			@foreach ($recommendations as $rec)
			<div class="col-md-4 product1">					
				<div class="shop_desc">

					<a href="{{url('detail/'.$rec->id)}}">
						<img src="{{asset($rec['image_url'])}}" width="100%" height="300px"  alt=""/>
					</a> <br>


					<h3><a href="{{url('detail/'.$rec->id)}}"></a><a href="{{url('detail/'.$rec->id)}}">{{$rec->name}}</a></h3>
					<p>{{$rec->start_on}} </p>						
					@if ($rec->price == 0)
					<span class="actual">Free</span><br>
					@else
					<span class="actual">Rp. {{$rec->price}}</span><br>
					@endif
					
					<ul class="buttons">
						@if (Auth::check())
						<li class="cart"><a href="{{url('detail/bookmark/'.$rec->id)}}">Add To Bookmark</a></li>
						@endif							
						<li class="shop_btn"><a href="{{url('detail/'.$rec->id)}}">Read More</a></li>
						<div class="clear"> </div>
					</ul>
				</div>
			</div>
			@endforeach
		</div>	
	</div>
</div>
</div>
<script>

//set marker
var lat = {{$event->latitude}};
var lng = {{$event->longitude}};


var fenway = {lat: lat, lng: lng};
var map = new google.maps.Map(document.getElementById('map'), {
	center: fenway,
	zoom: 14
});

var marker = new google.maps.Marker({
	position: {
		lat:lat,
		lng:lng
	},
	map: map,
	title : '{{$event->name}}',
});

var panorama = new google.maps.StreetViewPanorama(
	document.getElementById('pano'), {
		position: fenway,
		pov: {
			heading: 50,
			pitch: 10
		}
	});

map.setStreetView(panorama);


//determine user location
var initialLocation;
var siberia = new google.maps.LatLng(60, 105);
var newyork = new google.maps.LatLng(40.69847032728747, -73.9514422416687);
var browserSupportFlag =  new Boolean();

initialize();

function initialize() {

// Try W3C Geolocation (Preferred)
if(navigator.geolocation) {
	browserSupportFlag = true;
	navigator.geolocation.getCurrentPosition(function(position) {
		initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
		setupDirection(position.coords.latitude, position.coords.longitude);
	}, function() {
		handleNoGeolocation(browserSupportFlag);
	});
}
// Browser doesn't support Geolocation
else {
	browserSupportFlag = false;
	handleNoGeolocation(browserSupportFlag);
}

function handleNoGeolocation(errorFlag) {
	if (errorFlag == true) {
		alert("Geolocation service failed.");
		initialLocation = newyork;
	} else {
		alert("Your browser doesn't support geolocation.");
	}
}
}

//setup direction


function setupDirection(userlat, userlng){
	var directionsService = new google.maps.DirectionsService();
	var directionsDisplay = new google.maps.DirectionsRenderer();

	directionsDisplay.setMap(map);
	directionsDisplay.setPanel(document.getElementById('panel'));

	var userpos = new google.maps.LatLng(userlat, userlng);

	var request = {
		origin: userpos,
		destination: new google.maps.LatLng(lat, lng),
		travelMode: google.maps.DirectionsTravelMode.DRIVING
	};

	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
		}
	});
}

map.setCenter(femway);

</script>
@stop