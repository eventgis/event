<style>
	#map-canvas{
		width: 1170px;
		height: 500px;
	}
</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places"
type="text/javascript"></script>
@extends('layout')
@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li class="current"><a href="{{url('new')}}">All Event</a></li>
						    	
						    	@if (Auth::check())
						    	<li><a href="{{url('add')}}">Add Event</a></li>
						    	@else
						    	<li><a href="{{url('login')}}">Add Event</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li><a href="{{url('maps')}}">Maps</a></li>
								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')
<div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="row">
				<div class="col-md-9 single_left">
				   <div class="single_image">


									<img class="etalage_source_image" src="{{URL::asset($event->image_url) }}" width="100%" height="300px"/>
								
					    </div>
				        <!-- end product_slider -->
				        <div class="single_right">
				        	<h3>{{$event->name}}</h3>
				        	<p class="m_10">Acara-Jakarta.com – Java Jazz Festival 2016 akan kembali digelar pada 4-6 Maret 2016 mendatang. Para musisi jazz kenamaan Tanah Air dan dunia akan kembali meramaikan event jazz terbesar negeri ini, seperti Benny Likumahuwa, Dewa Budjana, Glenn Fredly, The Groove, Tohpati, Level 42, BadBadNotGood, Patti Austin, Robin Thicke dan masih banyak lagi.</p>
				        	<!--<ul class="options">-->
								<h4 class="m_12">Date</h4>
								<p class="m_10">{{$event->start_on}}</p>

								<h4 class="m_12">Price</h4>
								<p class="m_10">Rp. {{$event->price}}</p>



								<!--
								<li><a href="#">151</a></li>
								<li><a href="#">148</a></li>
								<li><a href="#">156</a></li>
								<li><a href="#">145</a></li>
								<li><a href="#">162(w)</a></li>
								<li><a href="#">163</a></li>
							</ul>
				        	<ul class="product-colors">
								<h3>available Colors</h3>
								<li><a class="color1" href="#"><span> </span></a></li>
								<li><a class="color2" href="#"><span> </span></a></li>
								<li><a class="color3" href="#"><span> </span></a></li>
								<li><a class="color4" href="#"><span> </span></a></li>
								<li><a class="color5" href="#"><span> </span></a></li>
								<li><a class="color6" href="#"><span> </span></a></li>
								<div class="clear"> </div>
							</ul>-->
							@if (Auth::check())
								
							<div class="btn_form">
							   <form action="attend" method="post">
								<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
							   	<input type="hidden" name="event_id" value="{{$event->id}}">
							   	<input type="hidden" name="user_id" value="{{$us->id}}">
								<input type="submit" value="Attend" title="" onclick="return confirm('Are you sure you want to attend this event ?')">
								
							  </form>
							</div>
								
							<div class="btn_form">
							   <form action="bookmark" method="post">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
							   	<input type="hidden" name="event_id" value="{{$event->id}}">
								<input type="hidden" name="user_id" value="{{$us->id}}">
								
								<input type="submit" value="Add to Bookmark" title="">
							   	
							  </form>
							</div>
							@endif


							<h4>{{$totalattendee->count()}} People Attend</h4>
							
							<div class="social_buttons">
								<button type="button" class="btn1 btn1-default1 btn1-twitter" onclick="">
					              <i class="icon-twitter"></i> Tweet
					            </button>
					            <button type="button" class="btn1 btn1-default1 btn1-facebook" onclick="">
					              <i class="icon-facebook"></i> Share
					            </button>
					             <button type="button" class="btn1 btn1-default1 btn1-google" onclick="">
					              <i class="icon-google"></i> Google+
					            </button>
					            <button type="button" class="btn1 btn1-default1 btn1-pinterest" onclick="">
					              <i class="icon-pinterest"></i> Pinterest
					            </button>
					        </div>
				        </div>
				        <div class="clear"> </div>
				</div>
				<!--<div class="col-md-3">
				  <div class="box-info-product">
					<p class="price2">$130.25</p>
					       <ul class="prosuct-qty">
								<span>Quantity:</span>
								<select>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
								</select>
							</ul>
							<button type="submit" name="Submit" class="exclusive">
							   <span>Add to cart</span>
							</button>
				   </div>
			   </div>-->
			</div>		
			<div class="desc">
				<br><br>
			   	<h4>Description</h4>
			   	<p align="justify">{{$event->description}}</p>
			</div>
			<div id="map-canvas"></div>
			<br><br>
			<div class="row">
				<h4 class="m_11">Recommendations Event</h4>

				@foreach ($recommendations as $rec)
				<div class="col-md-4 product1">					
					<div class="shop_desc">

						<a href="{{url('detail/'.$rec->id)}}">
						<img src="{{asset($rec['image_url'])}}" width="100%" height="300px"  alt=""/>
						</a> <br>


						<h3><a href="{{url('detail/'.$rec->id)}}"></a><a href="{{url('detail/'.$rec->id)}}">{{$rec->name}}</a></h3>
						<p>{{$rec->start_on}} </p>						
						@if ($rec->price == 0)
							<span class="actual">Free</span><br>
						@else
							<span class="actual">Rp. {{$rec->price}}</span><br>
						@endif
						
						<ul class="buttons">
						@if (Auth::check())
							<li class="cart"><a href="{{url('detail/bookmark/'.$rec->id)}}">Add To Bookmark</a></li>
						@endif							
							<li class="shop_btn"><a href="{{url('detail/'.$rec->id)}}">Read More</a></li>
							<div class="clear"> </div>
					    </ul>
				    </div>
				</div>
				@endforeach
			</div>	
	     </div>
	   </div>
	  </div>
<script>
var lat = {{$event->latitude}};
var lng = {{$event->longitude}};
var directionsService = new google.maps.DirectionsService;
var directionsDisplay = new google.maps.DirectionsRenderer;

var map= new google.maps.Map(document.getElementById('map-canvas'),{
	center:{
		lat:lat,
		lng:lng
	},
	zoom:15
});
directionsDisplay.setMap(map);

var infoWindow = new google.maps.InfoWindow({map: map});
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
      infoWindow.setContent('Your Location');
      map.setCenter(pos);
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }

 function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
  directionsService.route({
    origin: pos,
    destination: marker,
    travelMode: google.maps.TravelMode.DRIVING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}
</script>
	@stop