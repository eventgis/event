<nav class="navbar navbar-static-top" role="navigation">
	<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
	</a>
	<div class="navbar-custom-menu">
		<ul class="nav navbar-nav">
			<li class="dropdown notifications-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-bell-o"></i>
				<span class="label label-warning"></span>
				</a>
				<ul class="dropdown-menu">
					<li class="header">You have 0 notifications</li>
					<li>
						<ul class="menu">
						<li>
							<a href="#">
							<i class="ion ion-ios-people info"></i> Notification title
						</a>
						</li>
							...
						</ul>
					</li>
					<li class="footer"><a href="#">View all</a></li>
				</ul>
			</li>
			<li class="dropdown user user-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" >
				<img src="{{URL::asset('images/team1.jpg')}}" class="user-image" alt="User Image">
				<span class="hidden-xs">{{$us->name}}</span>
				</a>
				<ul class="dropdown-menu">
					<li class="user-header">
						<img src="{{URL::asset('images/team1.jpg')}}" class="img-circle" alt="User Image">
						<p>{{$us->name}}<small>Member since 2016</small></p>
					</li>
					<li class="user-footer">
						<div class="pull-left">
							<a href="#" class="btn btn-default btn-flat">Profile</a>
						</div>
						<div class="pull-right">
							<a href="{{action('Admin\LoginController@logout')}}" class="btn btn-default btn-flat">Sign out</a>
						</div>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</nav>