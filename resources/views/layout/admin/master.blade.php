<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Web Administrator Elgis</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link href="{{URL::asset('admin/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/plugins/datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/icon/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/icon/ionicon/css/ionicons.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/dist/css/skins/_all-skins.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/plugins/iCheck/flat/blue.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/plugins/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/dist/css/skins/_all-skins.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('admin/plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
		

		<script type="text/javascript" src="{{URL::asset('admin/plugins/jQuery/jQuery.min.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('admin/plugins/jQueryUI/jquery-ui.min.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('admin/dist/js/raphael/raphael-min.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('admin/plugins/morris/morris.min.js')}}"></script>

		<style type="text/css">
			.dataTables_wrapper .dataTables_paginate .paginate_button {
			    padding : 0px;
			    margin-left: 0px;
			    display: inline;
			    border: 0px;
			}
			.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
			    border: 0px;
			}
		</style>
		
		
	</head>
	<body class="hold-transition skin-purple sidebar-mini">
		
		<header class="main-header">
    		<a href="{{ url('adminpage/dashboard') }}" class="logo"><img src="{{URL::asset('images/elgis.png')}}" alt="" width="50px"/><!-- LOGO -->ELGIS</a>
			@include('layout.admin.include.navigasi')
  		</header>
	</div>
		@yield('content')
	</body>
	
	
	<script type="text/javascript" src="{{URL::asset('admin/bootstrap/js/bootstrap.min.js')}}"></script>
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/select2/select2.full.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/knob/jquery.knob.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/dist/js/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/plugins/fastclick/fastclick.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/dist/js/app.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/dist/js/pages/dashboard.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('admin/dist/js/demo.js')}}"></script>

</html>