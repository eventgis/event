@if(isset($user))
	@foreach($user as $us)
<div id="editUser_{{ $us->id }}" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" >Edit User</h4>
			</div>
			<form action="{{('user/.$us->id')}}" method="post">
				<div class="modal-body">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
                		<input type="text" name="name" class="form-control" value="{{$us->name}}"/>
              		</div>
					<div class="form-group">
						<label>Username</label>
                		<input type="text" name="username" class="form-control" value="{{$us->username}}"/>
              		</div>
              		<div class="form-group">
						<label>Email</label>
                		<input type="email" name="email" class="form-control" value="{{$us->email}}"/>
              		</div>
              		<div class="form-group">
						<label>Password</label>
                		<input type="password" name="password" class="form-control" value="{{$us->password}}"/>
              		</div>
              		<div class="form-group">
							<label>Role</label>
	                		<select name="role" class="form-control">
	                			<option value="{{$us->role}}">{{$us->role}}</option>
								<option value="admin">Admin</option>
								<option value="user">User</option>
							</select>
	              		</div>
				<div class="modal-footer">
						<div class="form-group">
							<input type="submit" class="btn bg-purple margin">
		          		</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endforeach
@endif