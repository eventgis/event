@extends('layout.admin.master')
@section('content')

<div class="wrapper">
	@include('admin.dashboard.include.sidebar')
	<div class="content-wrapper">
		<section class="content-header">
			<h1 >Master Data <small><strong>(User)</strong></small ></h1>
			<ol class="breadcrumb">
				<li><a href="{{ url('adminpage/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li><a href="{{ url('#') }}"><i class="fa fa-list-alt"></i> Master Data</a></li>
				<li class="active"><a href="{{ url('adminpage/user') }}"><i class="ion-pizza"></i> User</a></li>
			</ol>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">User List</h3>
				</div>
				<div class="box-body">
					
					<div class="panel panel-default">
						<div class="panel-body">
							<button type="button" class="btn bg-purple margin" data-toggle="modal" data-target="#newUser">Create User</button>
						</div>
					</div>


					<h2>Member Waiting List</h2>
					<table id="userTablewait" class="table table-condensed table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Username</th>
								<th>Email</th>
								<th>Role</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($user as $us)
								@if ($us->confirmed == '0')
								<tr>
									<td>{{$us->id}}</td>
									<td>{{$us->name}}</td>
									<td>{{$us->username}}</td>
									<td>{{$us->email}}</td>
									<td>{{$us->role}}</td>
									<td align="center">
											<a href="user/{{$us->id}}/reject" class="btn btn-sm bg-red margin">Reject</a>
									</td>
								</tr>
								@endif
							@endforeach
						</tbody>
					</table>
		




					<h2>User Confirmed</h2>
					<table id="userTable" class="table table-condensed table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Username</th>
								<th>Email</th>
								<th>Role</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($user as $us)
								@if ($us->confirmed == '1')
								<tr>
									<td>{{$us->id}}</td>
									<td>{{$us->name}}</td>
									<td>{{$us->username}}</td>
									<td>{{$us->email}}</td>
									<td>{{$us->role}}</td>
									
									<!--<td align="center">
									<button type="button" class="btn btn-sm bg-blue margin" data-toggle="modal" data-target="#editUser_{{ $us->id }}">Edit</button></td>-->
									<td align="center">
										<form action="{{('user/'.$us->id)}}" method="post">
											<input type="hidden" name="_method" value="delete">
											<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
											<input type="submit" href="" class="btn btn-sm bg-red margin" value="Delete">
										</form>
									</td>
								</tr>
								@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
	@include('admin.dashboard.include.footer')
	@include('admin.userpage.include.modal_createuser')
	@include('admin.userpage.include.modal_edituser')
</div>
<script type="text/javascript">
	$(function () {
    	$("#userTable").DataTable();
   	});
  //  	$('#editUser').on('show.bs.modal', function (user) {
  //  		var button = $('#editUser');
  //  		var isi = button.data('whatever');
  //  		var modal = $(this);
		// modal.find('.modal-body input').val(isi)
  //  	});
</script>
<script type="text/javascript">
	$(function () {
    	$("#userTablewait").DataTable();
   	});
  //  	$('#editUser').on('show.bs.modal', function (user) {
  //  		var button = $('#editUser');
  //  		var isi = button.data('whatever');
  //  		var modal = $(this);
		// modal.find('.modal-body input').val(isi)
  //  	});
</script>
@stop