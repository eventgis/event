@extends('layout.admin.master')
@section('content')

<style>
	#map-canvas {
		height: 300px;
		margin: 0px;
		padding: 0px
	}
</style>

<div class="wrapper">
	@include('admin.dashboard.include.sidebar')
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Event Detail<small>ELGIS</small></h1>
			<ol class="breadcrumb">
				<li><a href="{{ url('adminpage/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li><a href="{{ url('adminpage/masterdata/event') }}"><i class="fa fa-home"></i> Event </a></li>
				<li class="active"><a href="{{ url('adminpage/masterdata/event/'.$event->id)}}"><i class="fa fa-home"></i> {{$event->name}} Detail</a></li>
			</ol>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">{{$event->name}} Detail</h3>
				</div>
				<div class="box-body">
					
					<div class="col-md-6">
						<img src="{{URL::asset($event->image_url) }}" alt="{{($event->name)}}" width="100%" height="350px">	
					</div>
					<form action="{{($event->id)}}" id="formValue" method="post">
						<input type="hidden" name="_method" value="put">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
						<div class="col-md-6">
							<div class="form-group" align="center">
								<button type="button" class="btn bg-purple margin" id="editValue" style="width:80px;">Edit</button>
								<button type="submit" class="btn bg-purple margin" id="editValue" style="width:80px;">Save</button>
								<button type="button" class="btn btn-danger margin" id="cancelValue" style="width:80px;">Cancel</button>
							</div>
							<div class="form-group">
								<label>Aprroved</label>
								<select class="form-control" name="is_approved" disabled="">
									<option value="0">Not Approved</option>
									<option value="1" @if ($event->is_approved == 1) selected @endif>Approved</option>
								</select>
							</div>

							<div class="form-group">
								<label>Event Name</label>
								<input type="text" name="name" class="form-control" value="{{$event->name}}" disabled=""/>
							</div>
							
							<div class="form-group">
								<label>Longitude</label>
								<input type="text" name="longitude" id='long' class="form-control" value="{{$event->longitude}}" disabled=""/>
							</div>
							<div class="form-group">
								<label>Latitude</label>
								<input type="text" name="latitude" id="lat" class="form-control" value="{{$event->latitude}}" disabled=""/>
							</div>
							<div class="form-group">
								<label>Price</label>
								<input type="text" name="price" class="form-control" value="{{$event->price}}" disabled=""/>
							</div>
							
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control" name="description" style="height: 108px;" disabled="">{{$event->description}}</textarea>
							</div>
							<div class="form-group">
								<label>Organizer</label>
								<input type="text" name="organizer" class="form-control" value="{{$event->organizer}}" disabled=""/>
							</div>
							<div class="form-group">
								<label>Date</label>
								<input type="text" name="start_on" class="form-control" value="{{$event->start_on}}" disabled=""/>
							</div>	
						</div>
						<div class="col-md-10">
						<div id="map-canvas" disabled=""></div>
					</div>
					</form>
									</div>
			</div>
		</section>
	</div>
	@include('admin.dashboard.include.footer')
</div>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script type="text/javascript">
	$('#editValue').click(function () {
   		$('#formValue').find('input, textarea, select').prop('disabled', false);
   	});
   	$('#cancelValue').click(function () {
   		$('#formValue').find('input, textarea, select').prop('disabled', true);
   	});
   	

   	function initialize() {
		var map = {
			zoom: 15,
			center: new google.maps.LatLng({{$event->latitude}}, {{$event->longitude}})
		};
		map = new google.maps.Map(document.getElementById('map-canvas'),
		map);

		var pointer = new google.maps.Marker({
			position:{
				lat:{{$event->latitude}}, 
				lng:{{$event->longitude}}
			},
			map:map,
			draggable: true
		});

		google.maps.event.addListener(pointer, 'position_changed', function(){
			var longitude = pointer.getPosition().lng();
			var latitude = pointer.getPosition().lat();

			$('#long').val(longitude);
			$('#lat').val(latitude);
		});

		google.maps.event.addListener(mapOptions, 'position_changed', function(){
			var userLng = parseFloat($('#long').val());
			var userLat = parseFloat($('#lat').val());
		  
			var mapOptions = {
			    center: { lat: userLat, lng: userLng },
			    zoom: 8
			};
			var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions)
		});
	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>
@stop