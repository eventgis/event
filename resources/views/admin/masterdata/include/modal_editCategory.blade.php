@if(isset($category))
	@foreach($category as $cat)
		<div id="editCategory_{{$cat->id}}" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Edit Category Event</h4>
					</div>
					<form action="{{('category/'.$cat->id)}}" method="post">
						<div class="modal-body">
							<input type="hidden" name="_method" value="put">
							<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
							<div class="form-group">
								<label>Category Name</label>
		                		<input type="text" name="nama_kategori" class="form-control" value="{{$cat->category_name}}" />
		              		</div>
							<div class="modal-footer">
								<div class="form-group">
									<input type="submit" class="btn bg-purple margin">
			          			</div>
							</div>
						</div> 	
					</form>
				</div>
			</div>
		</div>
	@endforeach
@endif