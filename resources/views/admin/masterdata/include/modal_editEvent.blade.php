@if(isset($event))
	@foreach($event as $ev)
		<div id="editEvent_{{$ev->id}}" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Edit Event</h4>
					</div>
					<form action="{{('event/'.$ev->id)}}" method="post">
						<div class="modal-body">
							<input type="hidden" name="_method" value="put">
							<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
							<div class="form-group">
								<label>Event Name</label>
		                		<input type="text" name="nama_event" class="form-control" value="{{$ev->nama_event}}" />
		              		</div>			
              				<div class="form-group">
								<label>Location</label>
                				<input type="text" name="lokasi" class="form-control" value="{{$ev->lokasi}}"/>
              				</div>
              				<div class="form-group">
								<label>Date</label>
                				<input type="date" name="waktu" class="form-control" value="{{$ev->waktu}}"/>
              				</div>
              				<div class="form-group">
								<label>Promoter</label>
                				<input type="text" name="penyelenggara" class="form-control" value="{{$ev->penyelenggara}}"/>
              				</div>
              				<div class="form-group">
								<label>Price</label>
                				<input type="text" name="harga" class="form-control" value="{{$ev->harga}}"/>
              				</div>
              				<div class="form-group">
								<label>Description</label>
                				<input type="text" name="keterangan_event" class="form-control" value="{{$ev->keterangan_event}}"/>
              				</div>
              				<div class="form-group">
								<label>Poster</label>
                				<input type="file" name="poster" class="form-control" value="{{$ev->poster}}"/>
              				</div>

							<div class="modal-footer">
								<div class="form-group">
									<input type="submit" class="btn bg-purple margin">
			          			</div>
							</div>
						</div> 	
					</form>
				</div>
			</div>
		</div>
	@endforeach
@endif