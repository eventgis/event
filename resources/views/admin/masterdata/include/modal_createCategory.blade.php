<div id="newCategory" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" >Create New Category</h4>
			</div>
			<form action="{{('category')}}" method="post">
				<div class="modal-body">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
				<div class="modal-body">
					<div class="form-group">
						<label>Category Name</label>
                		<input type="text" name="category_name" class="form-control" />
              		</div>
				<div class="modal-footer">
						<div class="form-group">
							<input type="submit" class="btn bg-purple margin">
		          		</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>