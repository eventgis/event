@extends('layout.admin.master')
@section('content')

<div class="wrapper">
	@include('admin.dashboard.include.sidebar')
	<div class="content-wrapper">
		<section class="content-header">
			<h1 >Event Data <small><strong>(Category)</strong></small ></h1>
			<ol class="breadcrumb">
				<li><a href="{{ url('adminpage/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li><a href="{{ url('#') }}"><i class="fa fa-list-alt"></i> Event Data</a></li>
				<li class="active"><a href="{{ url('adminpage/user') }}"><i class="ion-pizza"></i> Category</a></li>
			</ol>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Event Category</h3>
				</div>
				<div class="box-body">
					
					<div class="panel panel-default">
						<div class="panel-body">
							<button type="button" id="editCategory" class="btn bg-purple margin" data-toggle="modal" data-target="#newCategory">Create Category</button>
						</div>
					</div>
				
	
					<h2>Category List</h2>
					<table id="categoryTable" class="table table-condensed table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Category Name</th>
								
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($category as $cat)
								<tr>
									<td>{{$cat->id}}</td>
									<td>{{$cat->category_name}}</td>
									
									<td align="center">
									<button type="button" class="btn btn-sm bg-purple margin" data-toggle="modal" data-target="#editCategory_{{ $cat->id }}">Edit</button>
										</td>
									<td align="center">
										<form action="{{('category/'.$cat->id)}}" method="post">
											<input type="hidden" name="_method" value="delete">
											<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
											<input type="submit" href="" class="btn btn-sm bg-red margin" value="Delete">
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
	@include('admin.dashboard.include.footer')
	@include('admin.masterdata.include.modal_createCategory')
	@include('admin.masterdata.include.modal_editCategory')
</div>
<script type="text/javascript">
	$(function () {
    	$("#categoryTable").DataTable();
   	});
  //  	$('#editCategory').on('show.bs.modal', function (category) {
  //  		var button = $('#editCategory');
  //  		var isi = button.data('whatever');
  //  		var modal = $(this);
		// modal.find('.modal-body input').val(isi)
  //  	});
</script>
@stop