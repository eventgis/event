@extends('layout.admin.master')
@section('content')

<div class="wrapper">
	@include('admin.dashboard.include.sidebar')
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Master Data <small><strong>(Attendees)</strong></small ></h1>
			<ol class="breadcrumb">
				<li><a href="{{ url('adminpage/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li><a href="{{ url('#') }}"><i class="fa fa-list-alt"></i> Master Data</a></li>
				<li class="active"><a href="{{ url('adminpage/masterdata/event') }}"><i class="ion-pizza"></i> Attendees</a></li>
			</ol>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Attendees List</h3>
				</div>
				<div class="box-body">
					
					<table id="attendeesTable" class="table table-condensed table-hover">
						<thead>
							<tr>
								<td>Event Name</td>
								<td>Total Attendee</td>
								
								<td></td>
							</tr>
						</thead>
						<tbody>
							@foreach($event as $ev)
								<tr>
									<td>{{$ev->name}}</td>
									<td>{{$ev->attendees->count()}}</td>
									
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
	@include('admin.dashboard.include.footer')
</div>
<script type="text/javascript">
	$(function () {
    	$("#eventTable").DataTable();
   	});
  //  	$('#editEvent').on('show.bs.modal', function (event) {
  //  		var button = $('#editEvent');
  //  		var isi = button.data('whatever');
  //  		var modal = $(this);
		// modal.find('.modal-body input').val(isi)
  //  	});
</script>
@stop