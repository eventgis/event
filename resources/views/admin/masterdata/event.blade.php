@extends('layout.admin.master')
@section('content')

<div class="wrapper">
	@include('admin.dashboard.include.sidebar')
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Master Data <small><strong>(Event)</strong></small ></h1>
			<ol class="breadcrumb">
				<li><a href="{{ url('adminpage/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li><a href="{{ url('#') }}"><i class="fa fa-list-alt"></i> Master Data</a></li>
				<li class="active"><a href="{{ url('adminpage/masterdata/event') }}"><i class="ion-pizza"></i> Event</a></li>
			</ol>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Event List</h3>
				</div>
				<div class="box-body">
					<div class="panel panel-default">
						<div class="panel-body">
							<a href="{{url('adminpage/masterdata/event/create')}}"><button type="button" class="btn bg-purple margin" data-toggle="modal">Create Event</button></a>
						</div>
					</div>
					<h2>Waiting List</h2>
						<table id="eventTablewait" class="table table-condensed table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Date</th>
								<th>Lat</th>
								<th>Long</th>								
								<th></td>
								<th></td>
							</tr>
						</thead>
						<tbody>
							@foreach($event as $ev)
								@if($ev->is_approved == '0')
								<tr>
									<td>{{$ev->id}}</td>
									<td>{{$ev->name}}</td>
									<td>{{$ev->start_on}}</td>
									<td>{{$ev->latitude}}</td>
									<td>{{$ev->longitude}}</td>
									<td align="center">
										<a href="{{'event/'.$ev->id}}"><button type="button" class="btn btn-sm bg-purple margin">View</button></a></td>
									<td align="center">
										<form action="{{('event/'.$ev->id)}}" method="post">
											<input type="hidden" name="_method" value="delete">
											<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
											<input type="submit" href="{{('event/'.$ev->id)}}" class="btn btn-sm bg-red margin" value="Delete">
										</form>
									</td>
								</tr>
								@endif
							@endforeach
						</tbody>
					</table>

					<h2>Event Approved</h2>
					<table id="eventTable" class="table table-condensed table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Date</th>
								<th>Lat</th>
								<th>Long</th>								
								<th></td>
								<th></td>
							</tr>
						</thead>
						<tbody>
							@foreach($event as $ev)
								@if($ev->is_approved == '1')
								<tr>
									<td>{{$ev->id}}</td>
									<td>{{$ev->name}}</td>
									<td>{{$ev->start_on}}</td>
									<td>{{$ev->latitude}}</td>
									<td>{{$ev->longitude}}</td>
									<td align="center">
										<a href="{{'event/'.$ev->id}}"><button type="button" class="btn btn-sm bg-purple margin">View</button></a></td>
									<td align="center">
										<form action="{{('event/'.$ev->id)}}" method="post">
											<input type="hidden" name="_method" value="delete">
											<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
											<input type="submit" href="{{('event/'.$ev->id)}}" class="btn btn-sm bg-red margin" value="Delete">
										</form>
									</td>
								</tr>
								@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
	@include('admin.dashboard.include.footer')
	@include('admin.masterdata.include.modal_createEvent')
	@include('admin.masterdata.include.modal_editEvent')
</div>
<script type="text/javascript">
	$(function () {
    	$("#eventTable").DataTable();
   	});
  //  	$('#editEvent').on('show.bs.modal', function (event) {
  //  		var button = $('#editEvent');
  //  		var isi = button.data('whatever');
  //  		var modal = $(this);
		// modal.find('.modal-body input').val(isi)
  //  	});
</script>

<script type="text/javascript">
	$(function () {
    	$("#eventTablewait").DataTable();
   	});
  //  	$('#editEvent').on('show.bs.modal', function (event) {
  //  		var button = $('#editEvent');
  //  		var isi = button.data('whatever');
  //  		var modal = $(this);
		// modal.find('.modal-body input').val(isi)
  //  	});
</script>
@stop