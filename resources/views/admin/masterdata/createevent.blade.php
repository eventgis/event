<style>
	#map-canvas{
		width: 350px;
		height: 250px;
	}
</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places" type="text/javascript"></script>
@extends('layout.admin.master')
@section('content')

<div class="wrapper">
	@include('admin.dashboard.include.sidebar')
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Event Detail<small>ELGIS</small></h1>
			<ol class="breadcrumb">
				<li><a href="{{ url('adminpage/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li><a href="{{ url('adminpage/user') }}"><i class="fa fa-home"></i>Event</a></li>
				<li class="active"><a href="{{ url('event/create')}}"><i class="fa fa-home"></i> Create</a></li>
			</ol>

<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Create Event</h3>
				</div>
				<div class="box-body">

					<form action="{{('addevent')}}" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}"> 
						<input type="hidden" name="is_approved" value="1">
						<div class="form-group">
						<label>Event Name</label>
                		<input type="text" name="name" class="form-control"/>
              		</div>
              		<div class="form-group">
							<label>Category</label>
	                		<select name="category" class="form-control">
	                			<option value="0">Select Category</option>
								@foreach($category as $cat)
								<option value="{{$cat->id}}">{{$cat->category_name}}</option>
								@endforeach
							</select>
	              		</div>
              		<div class="form-group">
							<label>Created By</label>
	                		<select name="created_by" class="form-control">
	                			<option value="0">Select User</option>
								@foreach($user as $us)
								@if ($us->role == 'admin')
								<option value="{{$us->id}}">{{$us->name}}</option>
								@endif
								@endforeach
							</select>
	              		</div>
              		<div class="form-group">
						<label>Search Location</label>
                		<input type="text" name="address" class="form-control" id="searchmap"/>
              			<div id="map-canvas"></div>
              		</div>
              		<div class="form-group">
						<label>Lat</label>
                		<input type="text" name="latitude" id="lat" class="form-control"/>
              		</div>

              		<div class="form-group">
						<label>Lng</label>
                		<input type="text" name="longitude" id="lng" class="form-control"/>
              		</div>

              		<div class="form-group">
						<label>Date</label>
                		<input type="datetime-local" name="start_on" class="form-control"/>
              		</div>
              		<div class="form-group">
						<label>Organizer</label>
                		<input type="text" name="organizer" class="form-control"/>
              		</div>
              		<div class="form-group">
						<label>Price</label>
                		<input type="text" name="price" class="form-control"/>
              		</div>
              		<div class="form-group">
						<label>Description</label>
                		<textarea name="description" class="form-control"/></textarea>
              		</div>
              		<div class="form-group">
						<label>Address</label>
                		<textarea name="address" class="form-control"/></textarea>
              		</div>
              		<div class="form-group">
						<label>Image</label>
                		<input type="file" name="image_url" class="form-control"/>
              		</div>
					<div class="modal-footer">
						<div class="form-group">
							<input type="submit" class="btn bg-purple margin">
		          		</div>
					</div>

					</form>
				</div>
			</div>
		</section>



		</section>
	</div>
	@include('admin.dashboard.include.footer')
</div>


<script>
var map= new google.maps.Map(document.getElementById('map-canvas'),{
	center:{
		lat:-6.371474900000001,
		lng:106.82445010000004
	},
	zoom:15
});

var marker = new google.maps.Marker({
	position: {
		lat:-6.371474900000001,
		lng:106.82445010000004
	},
	map: map,
	draggable:true
});

var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

google.maps.event.addListener(searchBox, 'places_changed', function(){
	var places = searchBox.getPlaces();
	var bounds = new google.maps.LatLngBounds();
	var i, place;

	for (i=0; place=places[i]; i++){
		bounds.extend(place.geometry.location);
		marker.setPosition(place.geometry.location);//set marker position new

	}

	map.fitBounds(bounds);
	map.setZoom(15);

});

google.maps.event.addListener(marker,'position_changed', function(){

	var lat = marker.getPosition().lat();
	var lng = marker.getPosition().lng();

	$('#lat').val(lat);
	$('#lng').val(lng);
});

</script>
@stop