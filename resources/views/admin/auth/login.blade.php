<!DOCTYPE HTML>
<html>
<head>
<title>Admin Login Form </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
<link href="{{url('admin/css/style1.css')}}" rel="stylesheet" type="text/css">
</head>
<body>	
		<div class="member-login">
				<form class="login" action="login" method="POST" >
	
					<div class="formtitle">
						<center><span><img src="{{asset('images/elgis.png')}}" width="500px"/>  </span>
						<b>ELGIS Login</b></center>
					</div>
					<div class="input">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="text" placeholder="Email" name="email"  required/> 
						<span><img src="{{asset('images/usr.png')}}"/> </span>
					</div>
					<div class="input">
						<input type="password" placeholder="Password" name="password" required/>
						<span><img src="{{asset('images/pas.png')}}"/></span>
					</div>
					<div class="buttons">
						<input class="bluebutton" type="submit" value="Login" />
					</div>
		
				</form>
		</div>
		
		</div>
			<p class="copy_right">&#169; 2016 Copyright by<a href="http://elgis.com" target="_blank">&nbsp;ELGIS</a> </p>
</body>
</html>
