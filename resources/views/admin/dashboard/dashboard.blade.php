@extends('layout.admin.master')
@section('content')

<div class="wrapper">

	@include('admin.dashboard.include.sidebar')
	
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Dashboard<small>ELGIS</small></h1>
			<ol class="breadcrumb">
				<li class="active"><a href="{{ url('adminpage/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			</ol>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-xs-12">
          			<div class="small-box bg-aqua">
            			<div class="inner">
             				<h3>{{$totalevent}}</h3>
							<p>EVENT</p>
            			</div>
            			<div class="icon">
              				<i class="ion ion-home"></i>
            			</div>
            			<a href="{{ url('adminpage/masterdata/event') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          			</div>
        		</div>
        		<div class="col-lg-6 col-md-6 col-xs-12">
          			<div class="small-box bg-yellow">
            			<div class="inner">
             				<h3>{{$totaluser}}</h3>
							<p>User</p>
            			</div>
            			<div class="icon">
              				<i class="ion ion-person-stalker"></i>
           				</div>
            			<a href="{{ url('adminpage/user') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          			</div>
        		</div>
				<section class="col-lg-7 connectedSortable">

				</section>
				<section class="col-lg-5 connectedSortable">

				</section>
			</div>
		</section>
	</div>

	@include('admin.dashboard.include.footer')

</div>

@stop