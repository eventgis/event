<aside class="main-sidebar">
	<section class="sidebar" style="height: auto;">
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{{URL::asset('images/team1.jpg')}}" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>{{$us->name}}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<ul class="sidebar-menu">
			<li class="treeview">
				<a href="{{ url('adminpage/user') }}"><i class="fa fa-users"></i> <span>User</span></a>
			</li>
			<li class="treeview">
				<a href="{{ url('#') }}"><i class="fa fa-list-alt"></i> <span> Event Data</span> <i class="fa fa-angle-down pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="{{ url('adminpage/masterdata/event') }}"><i class="ion-pizza"></i> All Event</a></li>
					<li><a href="{{ url('adminpage/masterdata/category') }}"><i class="ion-ios-home-outline"></i> Category Event </a></li>
					<li><a href="{{ url('adminpage/masterdata/attendee') }}"><i class="ion-ios-paper"></i> Attendee </a></li>
				</ul>
			</li>
			<!--<li class="treeview">
				<a href="{{ url('restaurant') }}"><i class="fa fa-home"></i> <span>Restaurant</span></i></a>
			</li>-->
			
		</ul>
	</section>
</aside>

