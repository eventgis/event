@extends('layout')

@section('menu')
<div class="menu">
	  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
	    <ul class="nav" id="nav">
	    	<li><a href="{{url('new')}}">All Event</a></li>
	    	@if (Auth::check())
	    	<li><a href="{{url('add')}}">Add Event</a></li>
	    	<li><a href="{{url('bookmarklist')}}">Bookmark</a></li>
	    	@else
	    	<li><a href="{{url('login')}}">Add Event</a></li>
	    	@endif
	    	<!--<li><a href="experiance.html">Experience</a></li>-->
	    	<li><a href="{{url('maps')}}">Maps</a></li>
			<li><a href="{{url('about')}}">About</a></li>								
			<div class="clear"></div>
		</ul>
		<script type="text/javascript" src="js/responsive-nav.js"></script>
</div>							

@stop

@section('content')

<div class="content-bottom">
<div class="container">
	<div class="row content_bottom-text">
	  <div class="col-md-7">
		<h3>Activate Your Account</h3>
		<p class="m_1">We've sent an email to your email address at {{$email}}.</p>
		 </div>
	</div>
</div>
</div>
 </div>
</div>
@stop