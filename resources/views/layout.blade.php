<head lang="en">
<title>Event Location GIS (ELGIS) </title>
<link href="{{url('css/bootstrap.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('css/style.css')}}" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="js/jquery.min.js"></script>
<!--<script src="js/jquery.easydropdown.js"></script>-->
<!--start slider -->
<link rel="stylesheet" href={{url('css/fwslider.css')}} media="all">
<script src="js/jquery-ui.min.js"></script>
<script src="js/fwslider.js"></script>
<!--end slider -->
<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>

 <!-- Add fancyBox main JS and CSS files -->
	<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
	<link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
		<script>
			$(document).ready(function() {
				$('.popup-with-zoom-anim').magnificPopup({
					type: 'inline',
					fixedContentPos: false,
					fixedBgPos: true,
					overflowY: 'auto',
					closeBtnInside: true,
					preloader: false,
					midClick: true,
					removalDelay: 300,
					mainClass: 'my-mfp-zoom-in'
			});
		});
		</script>

     <!--details-product-slider->
				<!--Include the Etalage files-->
					<link rel="stylesheet" href="css/etalage.css">
					<script src="js/jquery.etalage.min.js"></script>
				<!-- Include the Etalage files -->
				<script>
						jQuery(document).ready(function($){
			
							$('#etalage').etalage({
								thumb_image_width: 300,
								thumb_image_height: 400,
								
								show_hint: true,
								click_callback: function(image_anchor, instance_id){
									alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
								}
							});
							// This is for the dropdown list example:
							$('.dropdownlist').change(function(){
								etalage_show( $(this).find('option:selected').attr('class') );
							});

					});
				</script>

     @yield('header')
</head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
			  <div class="col-md-12">
				 <div class="header-left">
					 <div class="logo">
						<a href="{{url('home')}}"><img src="{{URL::asset('images/elgis.png') }}" alt="" width="75px"/></a>
					</div>
					 <section id="menu">
   						@yield('menu')
  					</section>
	    		    <div class="clear"></div>
	    	    </div>
	            <div class="header_right">
	    		  <!-- start search-->
				      <div class="search-box">
							<div id="sb-search" class="sb-search">
								<form>
									<input class="sb-search-input" placeholder="Search Your Event..." type="search" name="search" id="search">
									<input class="sb-search-submit" type="submit" value="">
									<span class="sb-icon-search"> </span>
								</form>
							</div>
						</div>
						<!--search-scripts-->
						<script src="js/classie.js"></script>
						<script src="js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
						<!--//search-scripts-->
				    <ul class="icon1 sub-icon1 profile_img">
					 <li><a class="active-icon c1" href="{{url('#')}}"> </a>
						<ul class="sub-icon1 list">
						  <div class="product_control_buttons">
						  	<a href="{{url('#')}}"><img src="{{URL::asset('images/edit.png')}}" alt=""/></a>
						  		<a href="{{url('#')}}"><img src="{{URL::asset('images/close_edit.png')}}" alt=""/></a>
						  </div>
						   <div class="clear"></div>
						   @if (Auth::check())
						  <li class="list_img"><img src="{{URL::asset('images/usr.png') }}" alt=""/></li>
						  <li class="list_desc"><h4><a href="{{url('add')}}">{{$us->name}}</a></h4><span class="actual">
                          Success Login</span></li>
						  @else
						  <li class="list_img"><img src="{{URL::asset('images/ev11.jpg')}}" alt=""/></li>
						  <li class="list_desc"><h4><a href="{{url('login')}}">Add Event</a></h4><span class="actual">
                          Free</span></li>
                          @endif
						  <div class="login_buttons">
							@if (Auth::check())
						    	<div class="login_button"><a href="{{url('attendlist')}}">Attend List</a></div>
								<div class="check_button"><a href="{{url('logout')}}">Sign Out</a></div>
							@else
								<div class="login_button"><a href="{{url('login')}}">Login</a></div>
							@endif						 							 	
						  </div>
						  <div class="clear"></div>
						</ul>
					 </li>
				   </ul>
		           <div class="clear"></div>
	       </div>
	      </div>
		 </div>
	    </div>
	</div>
	
	<section id="content">
    @yield('content')
  </section>

  <section id="suggest">
    @yield('suggest')
  </section>

		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Event</h4>
							<li><a href="#">New</a></li>
							<li><a href="#">Category</a></li>
							<li><a href="#">Add Event</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Category</h4>
							<li><a href="#">Music</a></li>
							<li><a href="#">Arts</a></li>
							<li><a href="#">Food & Drink</a></li>
							<li><a href="#">Photograpy</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>ELGIS</h4>
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">About Us</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Search</h4>
							<div class="footer_search">
				    		   <form>
				    			<input type="text" value="Search Your Event" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email';}"><input type="submit" value="Go">
				    		   </form>
					        </div>
							<ul class="social">	
							  <li class="facebook"><a href="#"><span> </span></a></li>
							  <li class="twitter"><a href="#"><span> </span></a></li>
							  <li class="instagram"><a href="#"><span> </span></a></li>	
							  <li class="pinterest"><a href="#"><span> </span></a></li>	
							  <li class="youtube"><a href="#"><span> </span></a></li>										  				
						    </ul>
		   					
						</ul>
					</div>
				</div>
				<div class="row footer_bottom">
				    <div class="copy">
			           <p> © 2016 All Right Reserved by <a href="http://elgis.com" target="_blank">ELGIS</a></p>
		            </div>
   				</div>
			</div>
		</div>
</body>	
</html>