@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="{{url('new')}}">All Event</a></li>
						    	@if (Auth::check())
						    	<li><a href="{{url('add')}}">Add Event</a></li>
						    	<li><a href="{{url('bookmarklist')}}">Bookmark</a></li>
						    	@else
						    	<li><a href="{{url('login')}}">Add Event</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li><a href="{{url('maps')}}">Maps</a></li>
								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')

<div class="main">
      <div class="shop_top">
	     <div class="container">
						<form action="register" method="POST"> 
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="register-top-grid">
										<h3>PERSONAL INFORMATION</h3>
										<div>
											<span>Name<label>*</label></span>
											<input type="text" name="name"> 
										</div>
										<div>
											<span>Username<label>*</label></span>
											<input type="text" name="username"> 
											<input type="hidden" name="role" value="user">
										</div>
										<div>
											<span>Email Address<label>*</label></span>
											<input type="text" name="email"> 
										</div>
										<div class="clear"> </div>
											<a class="news-letter" href="#">
												<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i> </i>Sign Up for Evind</label>
											</a>
										<div class="clear"> </div>
								</div>
								<div class="clear"> </div>
								<div class="register-bottom-grid">
										<h3>LOGIN INFORMATION</h3>
										<div>
											<span>Password<label>*</label></span>
											<input type="password" name="password">
										</div>
										<div>
											<span>Confirm Password<label>*</label></span>
											<input type="password" name="password">
										</div>
										<div class="clear"> </div>
								</div>
								<div class="clear"> </div>
								<div class="button2">
								<input type="submit" value="CREATE"></div>
						</form>
					</div>
		   </div>
	  </div>

		@stop