<style>
	#map-canvas{
		width: 350px;
		height: 250px;
	}
</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places"
type="text/javascript"></script>


					<div class="form-group">
						<label>Category</label>
                		<select>
							<option>Arts</option>
                			<option>Festival</option>
                			<option>Food & Drink</option>
                			<option>Music</option>
                			<option>Parties</option>
                			<option>Sports</option>
						</select>
					</div>
					<div class="form-group">
						<label>Event Name</label>
                		<input type="text" name="nama_event" class="form-control"/>
              		</div>
              		
              		<div class="form-group">
						<label>Search Location</label>
                		<input type="text" name="lokasi" class="form-control" id="searchmap"/>
              			<div id="map-canvas"></div>
              		</div>
              		<div class="form-group">
						<label>Lat</label>
                		<input type="text" name="lat" id="lat" class="form-control"/>
              		</div>

              		<div class="form-group">
						<label>Lng</label>
                		<input type="text" name="lng" id="lng" class="form-control"/>
              		</div>
		</div>
	</div>
</div>

<script>
var map= new google.maps.Map(document.getElementById('map-canvas'),{
	center:{
		lat:-6.371474900000001,
		lng:106.82445010000004
	},
	zoom:15
});

var marker = new google.maps.Marker({
	position: {
		lat:-6.371474900000001,
		lng:106.82445010000004
	},
	map: map,
	draggable:true
});

var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

google.maps.event.addListener(searchBox, 'places_changed', function(){
	var places = searchBox.getPlaces();
	var bounds = new google.maps.LatLngBounds();
	var i, place;

	for (i=0; place=places[i]; i++){
		bounds.extend(place.geometry.location);
		marker.setPosition(place.geometry.location);//set marker position new

	}

	map.fitBounds(bounds);
	map.setZoom(15);

});

google.maps.event.addListener(marker,'position_changed', function(){

	var lat = marker.getPosition().lat();
	var lng = marker.getPosition().lng();

	$('#lat').val(lat);
	$('#lng').val(lng);
});

</script>