@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="{{url('new')}}">All Event</a></li>
						    	@if (Auth::check())
						    	<li><a href="{{url('add')}}">Add Event</a></li>
						    	<li><a href="{{url('bookmarklist')}}">Bookmark</a></li>
						    	@else
						    	<li><a href="{{url('login')}}">Log In</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li><a href="{{url('maps')}}">Maps</a></li>
								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')
<!--<hr>
<div class="row">
	<div class="medium-3 columns text-right">
		<select>
			<option value="">Category</option>
			<option value="music">Music</option>
			<option value="arts">Arts</option>
			<option value="food">Food & Drink</option>
			<option value="sport">Sports</option>
		</select>
	</div>
	<div class="medium-9 columns">
		<div class="input-group">
	        <input class="input-group-field" type="text" placeholder="Search Event in Here........">
	        <div class="input-group-button">
	          <input type="submit" class="button" value="Search">
	        </div>
	      </div>
	</div>
</div>
<hr>
-->

	<div class="banner">
	<!-- start slider -->
	

       <div id="fwslider">

         <div class="slider_container">
            <div class="slide"> 
                <!-- Slide image -->
               <img src="images/slider1.jpg" class="img-responsive" alt=""/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">ELGIS</h1>
                        <!-- /Text title -->
                        <div class="button"><a href="#">See More</a></div>
                    </div>
                </div>
              
               <!-- /Texts container -->
            </div>
            
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
       </div>
       <!--/slider -->
      </div>
	  <div class="main">
		<div class="content-top">
			<h2>New Event</h2>
			<ul id="flexiselDemo3">
				@foreach ($event as $ev)
					@if ($ev->is_approved == '1')
				<li><img src="{{URL::asset($ev->image_url) }}" height="200px"/><a href="{{'detail/'.$ev->id}}"><h4>{{$ev->name}}</h4></a></li>
					@endif
				@endforeach
				
			</ul>
			<script type="text/javascript">
		$(window).load(function() {
			$("#flexiselDemo3").flexisel({
				visibleItems: 5,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
		    	responsiveBreakpoints: { 
		    		portrait: { 
		    			changePoint:480,
		    			visibleItems: 1
		    		}, 
		    		landscape: { 
		    			changePoint:640,
		    			visibleItems: 2
		    		},
		    		tablet: { 
		    			changePoint:768,
		    			visibleItems: 3
		    		}
		    	}
		    });
		    
		});
		</script>
		<script type="text/javascript" src="js/jquery.flexisel.js"></script>
		</div>
	</div>
	<div class="content-bottom">
		<div class="container">
			<div class="row content_bottom-text">
			  <div class="col-md-7">
				<h3>About ELGIS</h3>
				<p class="m_1">ELGIS is an application finding event with a Geographic Information System Technology. Elgis display information and location from various events category such as concert, celebrations, festival and etc. Information included name, date, description and location of event.</p>
				 </div>
			</div>
		</div>
	</div>
		 </div>
	    </div>
		@stop