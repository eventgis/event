@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="{{url('new')}}">All Event</a></li>
						    	@if (Auth::check())
						    	<li><a href="{{url('add')}}">Add Event</a></li>
						    	@else
						    	<li><a href="{{url('login')}}">Add Event</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li class="current"><a href="{{url('maps')}}">Maps</a></li>
								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')
{{$map['js']}}
{{$map['html']}}
<div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="row shop_box-top">
				
				{{$ev->count()}}

@foreach($ev as $row)
		{{HTML::image('uploads/icon/'.$row->icon)}}
		<a href="{{URL::to('web/kategori/'.$row->category)}}">
			{{$row->name}}
		</a>
@endforeach
              			
			
			</div>
		 </div>
	   </div>
	  </div>  

		@stop
