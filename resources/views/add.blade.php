<style>
	#map-canvas{
		width: 1140px;
		height: 500px;
	}
</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places"
type="text/javascript"></script>
@extends('layout')

@section('menu')
<div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="{{url('new')}}">All Event</a></li>
						    	@if (Auth::check())
						    	<li class="current"><a href="{{url('add')}}">Add Event</a></li>
						    	<li><a href="{{url('bookmarklist')}}">Bookmark</a></li>
						    	@else
						    	<li class="current"><a href="{{url('login')}}">Add Event</a></li>
						    	@endif
						    	<!--<li><a href="experiance.html">Experience</a></li>-->
						    	<li><a href="{{url('maps')}}">Maps</a></li>
								<li><a href="{{url('about')}}">About</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							

@stop

@section('content')


<div class="main">
      <div class="shop_top">
	     <div class="container">
	     	<h3><center>CREATE EVENT</center></h3><br>
						<form action="add" method="POST" enctype="multipart/form-data"> 
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
					<div class="form-group">
						<label>Event Name</label>
                		<input type="text" name="name" class="form-control"/>
              		</div>
              	<div class="form-group">
							<label>Category</label>
	                		<select name="category" class="form-control">
	                			<option value="0">Select Category</option>
								@foreach($category as $cat)
								<option value="{{$cat->id}}">{{$cat->category_name}}</option>
								@endforeach
							</select>
	              		</div>
	              	<div class="form-group">
							<label>Created By</label>
	                		<select name="created_by" class="form-control">
							
								<option value="{{$us->id}}">{{$us->name}}</option>
								
							</select>
	              		</div>
              		<div class="form-group">
						<label>Search Location</label>
                		<input type="text" name="address" class="form-control" id="searchmap"/>
              			<div id="map-canvas"></div>
              		</div>
              		<div class="form-group">
						<label>Lat</label>
                		<input type="text" name="latitude" id="lat" class="form-control"/>
              		</div>

              		<div class="form-group">
						<label>Lng</label>
                		<input type="text" name="longitude" id="lng" class="form-control"/>
              		</div>

              		<div class="form-group">
						<label>Date</label>
                		<input type="datetime-local" name="start_on" class="form-control"/>
              		</div>
              		<div class="form-group">
						<label>Organizer</label>
                		<input type="text" name="organizer" class="form-control"/>
              		</div>
              		<div class="form-group">
						<label>Price</label>
                		<input type="text" name="price" class="form-control"/>
              		</div>
              		<div class="form-group">
						<label>Description</label>
                		<textarea name="description" class="form-control"/></textarea>
              		</div>
              		<div class="form-group">
						<label>Image</label>
                		<input type="file" name="image_url" class="form-control"/>
              		</div>
								<div class="button2">
								<input type="submit" value="CREATE"></div>
						</form>
					</div>
		   </div>
	  </div>




<script>
var map= new google.maps.Map(document.getElementById('map-canvas'),{
	center:{
		lat:-6.371474900000001,
		lng:106.82445010000004
	},
	zoom:15
});

var marker = new google.maps.Marker({
	position: {
		lat:-6.371474900000001,
		lng:106.82445010000004
	},
	map: map,
	draggable:true
});

var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

google.maps.event.addListener(searchBox, 'places_changed', function(){
	var places = searchBox.getPlaces();
	var bounds = new google.maps.LatLngBounds();
	var i, place;

	for (i=0; place=places[i]; i++){
		bounds.extend(place.geometry.location);
		marker.setPosition(place.geometry.location);//set marker position new

	}

	map.fitBounds(bounds);
	map.setZoom(15);

});

google.maps.event.addListener(marker,'position_changed', function(){

	var lat = marker.getPosition().lat();
	var lng = marker.getPosition().lng();

	$('#lat').val(lat);
	$('#lng').val(lng);
});

</script>
		@stop