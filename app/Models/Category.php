<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
            'category_name'
    ];

    protected $hidden = [
        
    ];

  public function event(){
        return $this->hasMany('App\Models\Event', 'category');
    }
}
