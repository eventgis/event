<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'name',
            'description',
            'start_on',
            'price',
            'created_by',
            'organizer',
            'address',
            'is_approved',
            'is_done',
            'category',
            'image_url',
            'latitude',
            'longitude',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    protected $dates = ['deleted_at'];

    public function user(){
        return $this->belongsTo('App\Models\User', 'created_by')->select(['id', 'name']);
    }

    public function attendees(){
        return $this->hasMany('App\Models\Attendee', 'event_id');
    }

    public function categori(){
        return $this->belongsTo('App\Models\Category', 'category')->select(['id', 'category_name']);
    }
}
