<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'event_id',
			'user_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function event(){
        return $this->belongsTo('App\Models\Event', 'event_id')->with('categori');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id')->select(['id', 'name']);
    }
}
