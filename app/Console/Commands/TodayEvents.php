<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Event;
use DB;
use Carbon\Carbon;
use Log;

class TodayEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'events:todayevents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for events helds today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::now();
        $event = Event::where('start_on', $today->toDateString())->get();
        
        foreach ($event as $e) {
            $e->is_done = 1;
            $e->save();
        }

        $this->info($today->toDateString()."  ".$event->count());
        Log::info('Checking today finished events: '.$today->toDateString()."  ".$event->count());
    }
}
