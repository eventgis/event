<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {

	Route::get('/', 'homeController@index');
	Route::get('/activate', 'homeController@activate');
	Route::get('/home', 'homeController@index');
	Route::get('/new', 'newController@index');
	Route::get('new/{id}', 'newController@search');
	Route::get('/category', 'eventController@category');
	Route::get('/category{id}', 'eventController@kategori');
	Route::get('/login', 'eventController@login');
	Route::get('/add', 'eventController@addevent');
	Route::post('detail/bookmark', 'detailController@bookmark');
	Route::post('detail/attend', 'detailController@attend');
	Route::post('/add', 'eventController@create');
	Route::post('/login', 'Auth\AuthController@login');
	Route::get('logout', 'Auth\AuthController@logout');
	Route::get('/register', 'eventController@register');
	Route::post('/register', 'Auth\AuthController@create');
	Route::get('/about', 'eventController@about');
	Route::get('/detail', 'detailController@index');
	Route::get('/detail/{id}' , 'detailController@show');
	Route::get('/maps', 'mapsController@index');
	Route::get('/tesmap', 'eventController@tesmap');
	Route::get('/bookmarklist', 'bookmarkController@index');
	Route::get('bookmarklist/{id}/delete', 'bookmarkController@delete');
	Route::get('/attendlist', 'attendlistController@index');
	Route::get('attendlist/{id}/cancel', 'attendlistController@cancel');
	Route::get('/todayevents', 'eventController@todayEvents');
	
	Route::get('/register/activate/{confirmationCode}', ['as' => 'activate', 'uses' => 'Auth\AuthController@confirm']);
	/*
	|--------------------------------------------------------------------------
	| ADMIN Routes
	|--------------------------------------------------------------------------
	|
	| This route is used by the admin panel web app to manipulate resources in the database
	| The prefix starts with /admin
	*/
	Route::group(['prefix' => 'adminpage'], function(){

		Route::get('/login', ['as' => 'login', 'uses' => 'eventController@adminlogin']);

		Route::post('/login', ['uses' => 'Admin\LoginController@login']);

		Route::get('/logout', 'Admin\LoginController@logout');

		Route::get('/', function(){
			return redirect()->route('login');
		});	

		Route::group(['middleware' => 'admin'], function(){
			
			Route::resource('dashboard', 'Admin\DashboardController');

			Route::resource('user', 'Admin\User\UserController');

			Route::resource('user/{id}/approve', 'Admin\User\UserController@approve');

			Route::get('user/{id}/reject', 'Admin\User\UserController@reject');

			Route::group(['prefix' => 'masterdata'], function () {
			    Route::resource('event', 'Admin\MasterData\eventController');
			    Route::post('event/addevent', 'Admin\MasterData\eventController@addevent');

				Route::resource('category', 'Admin\MasterData\categoryController');

			    Route::resource('attendee', 'Admin\MasterData\attendeeController');
			});
		});
		
	});
});


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| This route is used by the mobile app to get application resource from the web
| The prefix starts with /api/v1
*/

Route::group(['prefix' => '/api/v1'],  function()
{
	Route::post('login', 'Api\AuthController@login');

	Route::post('users', 'Api\UserController@store');

	Route::get('events/{limit?}/{offset?}', 'Api\EventController@index');

	Route::get('event_recommendation/{id}', 'Api\EventController@show');
	
	Route::group(['middleware' => 'jwt.auth'], function()
	{
		Route::post('logout', 'Api\AuthController@logout');

		Route::resource('users', 'Api\UserController', ['except' => ['store']]);

		Route::resource('events', 'Api\EventController', ['except' => ['index', 'show']]);

		Route::resource('saved_events', 'Api\SavedEventController');

		Route::get('saved_events/user/{userid}', 'Api\SavedEventController@getByUser');

		Route::post('remove_saved', 'Api\SavedEventController@removeSaved');

		Route::resource('attendance', 'Api\AttendeeController');

		Route::get('attendance/user/{userid}', 'Api\AttendeeController@getByUser');

		Route::post('cancel_attendance', 'Api\AttendeeController@cancelAttendance');
	});
	
});
