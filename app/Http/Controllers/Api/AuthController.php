<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        //TO::DO JWT Auth
    	$credentials = $request->only('email', 'password');
        $credentials['confirmed'] = 1;

    	try {
    		//verify and create token
    		if(!$token = JWTAuth::attempt($credentials))
    		{
    			//wrong
                $success = "false";
    			return response()->json(['error' => 'invalid credentials', 'success' => 'false', 401]);
    		}
            else
            {
                if (Auth::attempt($credentials)) {
                    $user = Auth::user();

                    if($user->confirmed == 1)
                    {
                        $success = "true";
                        $message = "login successful";
                        return response()->json(compact('success', 'message', 'token', 'user'));
                    }
                    else{
                        $success = "false";
                        $message = "inactive account";
                        return response()->json(compact('success', 'message', 'token', 'user'));
                    }
                }
                else
                {
                    $success = "false";
                    $message = "login failed";
                    return response()->json(compact('success', 'message', 'token'));
                }                            
            }
    	} catch (JWTException $e) {
    		//failed
    		return response()->json(['error' => 'could_not_create_token'], 500);
    	}
    }

    public function logout(Request $request) 
    {
        $this->validate($request, [
            'token' => 'required' 
        ]);
        
        JWTAuth::invalidate($request->input('token'));
    }
}
