<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests;
use JWTAuth;
use Validator;
use Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return response()->json($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required',
            'email' => 'email|required',
            'password' => 'required'
            ]);

        if($validator->fails()){
            return "Incomplete data";
        }

        $input = $request->all();
        
        //Find for existing user
        $user = User::where('username', $input['username'])->orWhere('email', $input['email'])->get()->isEmpty();

        if(User::where('username', $input['username'])->orWhere('email', $input['email'])->get()->isEmpty())
        {            
            $input['confirmation_code'] = str_random(30);                            
            $newuser = User::create($input);        

            $this->send($newuser);

            return 'success';
        }
        else
        {
            return 'already exist';
        }
    }

    protected function send($user)
    {
        Mail::later(5, 'emails.confirmation', ['user' => $user], function($pesan) use($user) {
            $pesan->to($user['email'], $user['name'])
            ->subject('ELGIS account activation');
        });       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();        
        
        //Find for existing user
        $user = User::find($id);

        $user->name = $input['name'];

        $user->save();

        return $user->name;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
