<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\User;
use App\Http\Requests;
use DB;
use JWTAuth;
use Carbon\Carbon;

class EventController extends Controller
{
    private $token = "";

    /*
    Check for user token in the request.
    */
    public function setToken() 
    {
        $this->token = JWTAuth::parseToken('bearer', 'HTTP_AUTHORIZATION')->getToken();

        if(!$this->token)
        {
            throw new BadRequestHtttpException('Token not provided');
        }
        try
        {
            $this->token = JWTAuth::refresh($this->token);
        }
        catch(TokenInvalidException $e)
        {
            throw new AccessDeniedHttpException('The token is invalid');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($limit = 15, $offset = 0)
    {
        $events = Event::where('is_approved', 1)->with('user', 'attendees', 'categori')->orderBy('created_at', 'desc')->skip($offset)->take($limit)->get();

        if($events)
        {
            $success = "true";
        }
        else
        {
            $success = "false";
        }

        return response()->json(compact('success', 'events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = [
            'name' => 'abcde',
            'description' => 'abcde',
            'start_on' => '10-20-2010',
            'price' => 10000,
            'attendee' => 20,
            'organizer' => 'abcde',
            'address' => 'abcde',
            'is_done' => false,
            'image_url' => 'abcde'
        ];

        Event::create($event);

        return 'ok';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::with('categori')->find($id);
        $allevent = Event::with('user', 'attendees', 'categori')->where('is_approved', 1)->get();

        //geolocation
        /*$geolocation = $event->latitude.','.$event->longitude;
        $request = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false'; 
        $file_contents = file_get_contents($request);
        $geocode = json_decode($file_contents);
        $address = $geocode->results[0]->address_components;
        foreach ($address as $a) {
            if(in_array('administrative_area_level_2', $a->types))
            {
                $city = $a->long_name;
            }
        }*/

        $recommendations = [];
        $scores = [];

        if($event)
        {
            $success = "true";            

            foreach ($allevent as $e) {

                if($e->id != $event->id && $e->is_done != 1)
                {
                    $score = 0;
                    if($event->category == $e->category)
                    {
                        $score += 2;                        
                    }

                    if($event->price == $e->price || ($event->price > 0 && $e->price > 0))
                    {
                        $score += 1;
                    }

                    if(Carbon::parse($event->start_on)->month == Carbon::parse($e->start_on)->month)
                    {
                        $score += 1;
                    }
                    

                    //location check
                    /*$geolocation = $e->latitude.','.$e->longitude;
                    $request = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false'; 
                    $file_contents = file_get_contents($request);
                    $geocode = json_decode($file_contents);
                    $address = $geocode->results[0]->address_components;
                    foreach ($address as $a) {
                        if(in_array('administrative_area_level_2', $a->types))
                        {
                            $citycompar = $a->long_name;
                        }
                    }

                    if($city == $citycompar)
                    {
                        $score += 1;
                    }*/

                    $candidates[] = ['score' => $score, 'event' => $e];
                    $scores[] = $score;
                }                
            }
            
            sort($candidates);
            sort($scores);
            $total = count($candidates);

            for($i=0; $i<5; $i++)
            {
                if($candidates != null)
                {
                    $best = max($candidates);
                
                    $recommendations[] = $best;
                    array_pop($candidates);
                }                
            }            
        }
        else
        {
            $success = "false";
        }            

        return response()->json(compact('success', 'event', 'recommendations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::find($id);

        $input = $request->all();

        $event->name = $input['name'];

        $event->save();

        return response()->json($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
