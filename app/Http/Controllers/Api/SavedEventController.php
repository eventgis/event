<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Exceptions\Handler;
use Exceptions;
use App\Models\SavedEvent;
use App\Models\Attendee;
use App\Models\User;
use App\Models\Event;
use DB;
use Carbon\Carbon;
use JWTAuth;

class SavedEventController extends Controller
{
    private $token = "";

    /*
    Check for user token in the request.
    */
    public function setToken() 
    {
        $this->token = JWTAuth::parseToken('bearer', 'HTTP_AUTHORIZATION')->getToken();

        if(!$this->token)
        {
            throw new BadRequestHtttpException('Token not provided');
        }
        try
        {
            //$this->token = JWTAuth::refresh($this->token);
        }
        catch(TokenInvalidException $e)
        {
            throw new AccessDeniedHttpException('The token is invalid');
        }
    }

    public function getByUser($id)
    {
        /*$token = "";
        $this->setToken();
        $token = $this->token;*/

        /*$savedEvents = DB::table('events')
        ->leftJoin('attendees', 'attendees.event_id', '=', 'events.id')
        ->leftJoin('saved_events', 'saved_events.event_id', '=', 'events.id')
        ->where('saved_events.user_id', $id)
        ->select(DB::raw('saved_events.id as saved_id, events.*, COUNT(attendees.user_id) as attendee'))
        ->groupBy('saved_events.id')
        ->get();*/

        $savedEvents = SavedEvent::where('user_id', $id)->with('event')->get();
        foreach ($savedEvents as $key => $saved) {
            $saved->event->user;
            $saved->event->categori;
            $saved->event->attendees;
        }

        if($savedEvents)
        {
            $success = "true";
        }
        else
        {
            $success = "false";
        }

        return response()->json(compact('success', 'savedEvents'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$token = "";
        $this->setToken();
        $token = $this->token;*/

        $input = $request->all();
        $event = Event::find($input['event_id']);
        $input['remind_on'] = Carbon::parse($event->start_on)->subDays(5);

        try {
            SavedEvent::create($input);
            $success = "true";
            $message = "Event saved";
        } catch (\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062){
                $success = "false";
                $message = "Event already saved";
            }
            else{
                $success = "false";
                $message = "Failed to save event";
            }
        }

        return response()->json(compact('success', 'message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removeSaved(Request $request)
    {
        /*$token = "";
        $this->setToken();
        $token = $this->token;*/

        $input = $request->all();
        $event = $input['event_id'];
        $user = $input['user_id'];

        try {
            SavedEvent::where('user_id', $user)->where('event_id', $event)->delete();
            $success = "true";
            $message = "Event removed";
        } catch (\Illuminate\Database\QueryException $e) {
            $success = "false";
            $message = "Failed to remove";
        }

        return response()->json(compact('success', 'message'));
    }
}
