<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required',
            'email' => 'email|required',
            'password' => 'required'
            ]);

        if($validator->fails()){
            return "Incomplete data";
        }
        
        $confirmation_code = str_random(30);

        $data = ['name' => $request->name,
         'username' => $request->username,
         'email' => $request->email,
         'password' => $request->password,
         'confirmation_code' => $confirmation_code];

        $user = User::create($data);
        $user->confirmation_code = $confirmation_code;
        $user->save();
        
        $this->send($user);
        return view('activate')->with('email', $user['email']);

    }

    protected function login(Request $request)
    {
                           
        $credentials = $request->only('email', 'password');
        $credentials['confirmed'] = 1;

        if (Auth::attempt($credentials)) 
        {
            $us = Auth::user();
            $success = "true";
            return redirect()->action('homeController@index')->with(['us'=>$us]);
        }
        else
        {
            $success = "false";
            return redirect()->action('eventController@login');
        } 
    }

    protected function logout()
    {
        Auth::logout();

        return redirect()->action('eventController@login');
    }

    protected function send($user)
    {

        Mail::send('emails.confirmation', ['user' => $user], function($pesan) use($user) {
            $pesan->to($user['email'], $user['name'])
            ->subject('ELGIS account activation');
        });       
    }

    public function confirm($confirmation_code)
    {
        if(!$confirmation_code)
        {
            return "invalid confirmation code";
        }

        $user = User::where('confirmation_code', $confirmation_code)->first();

        if ( ! $user)
        {
            return "invalid confirmation code";
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return 'You have successfully verified your account';
    }
}
