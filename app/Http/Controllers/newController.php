<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Category;
use Auth;
use Input;

class newController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('admin.userpage.userpage');
        $us = Auth::user();
        $category = Category::all();
        if (Input::has('category')) {

            if (Input::get('category')== 0){
            $event = Event::with('user', 'attendees', 'categori')->orderBy('start_on','desc')->paginate(12);
            $kat = 0;    
            } 

            else {
            $event = Event::with('user', 'attendees', 'categori')->where('category','=',Input::get('category'))->orderBy('start_on','desc')->paginate(12);
            $kat = Input::get('category');
            }
        }

        else {
            $event = Event::with('user', 'attendees', 'categori')->orderBy('start_on','desc')->paginate(12);
            $kat = 0;
            
        }

        return view('/new')->with(['us'=>$us, 'event'=>$event, 'category'=>$category, 'kat'=>$kat]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        event::create($input);
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event=event::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $event = event::find($id);
        $event->update($input);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=event::find($id);
        $event->delete();
        return redirect()->back();
    }
}
