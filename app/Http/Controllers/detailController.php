<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\User;
use App\Models\Attendee;
use App\Models\SavedEvent;
use Auth;
use Carbon\Carbon;

class detailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('admin.userpage.userpage');
        $us = User::Auth();
        $event = Event::with('user','categori','attendees');
        
        return view('/detail')->with(['event'=>$event]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function attend(Request $request)
    {
        $input = $request->all();
        Attendee::create($input);
        
        return redirect()->back();
    }

     public function bookmark(Request $request)
    {
        
        $input = $request->all();

        SavedEvent::create($input);
        
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        Event::create($input);
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $us = Auth::user();
        $uscek = Auth::check();
        $event = Event::with('categori')->find($id);
        $totalattendee = Attendee::where('event_id', '=', $id);

        $allevent = Event::where('is_approved', 1)->with('user', 'attendees', 'categori')->get();

        //$attend = Attendee::where('event_id', $id )->OrWhere('user_id', $us->id );
        //$bm = SavedEvent::where('event_id',  $id )->OrWhere('user_id', $us->id );
        //$att = Attendee::with('event', 'user')->get();
        //$bookmark = SavedEvent::with('event', 'user')->get();

        // Recommendations System with Content Based Filtering

        $recommendations = [];
        $scores = [];

        if($event)
        {       
            foreach ($allevent as $e) {

                if($e->id != $event->id)
                {
                    $score = 0;
                    if($event->category == $e->category)
                    {
                        $score += 2;                        
                    }

                    if($event->price == $e->price || ($event->price > 0 && $e->price > 0))
                    {
                        $score += 1;
                    }

                    if(Carbon::parse($event->start_on)->month == Carbon::parse($e->start_on)->month)
                    {
                        $score += 1;
                    }


                    $candidates[] = ['score' => $score, 'event' => $e];
                    $scores[] = $score;
                }                
            }

            sort($candidates);
            sort($scores);
            $total = count($candidates);

            for($i=0; $i<3; $i++)
            {
                $best = max($candidates);
                
                $recommendations[] = $best['event'];
                array_pop($candidates);
            }

        }
        else
        {

        }

        if($uscek){
        $attend= Attendee:: where('event_id', $id)->where('user_id', $us->id);
        $bookmark= SavedEvent:: where('event_id', $id)->where('user_id', $us->id);
        return view('detail')
            ->with(['event'=>$event,
                'totalattendee'=>$totalattendee, 
                'us'=>$us,
                'recommendations' => $recommendations,
                'attend'=>$attend, 'bookmark'=>$bookmark]);
        }
        else
        {

        }

         return view('detail')
            ->with(['event'=>$event,
                'totalattendee'=>$totalattendee, 
                'us'=>$us,
                'recommendations' => $recommendations
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $event = Event::find($id);
        $event->update($input);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=Event::find($id);
        $event->delete();
        return redirect()->back();
    }
}
