<?php

namespace App\Http\Controllers\Admin\MasterData;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\User;
use App\Models\Category;

use Illuminate\Validation;
Use Auth;
Use Validator;
Use Input;
Use File;

class eventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $us = Auth::user();
        $event = event::all();
        return view('admin.masterdata.event')->with(['event'=>$event, 'us'=>$us]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $us = Auth::user();
        $event = event::all();
        $user = User::all();
        $category = Category::all();
        return view('admin.masterdata.createevent')->with(['event'=>$event, 'user'=>$user, 'us'=>$us, 'category'=>$category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addevent(Request $request)
    {
        $input = $request->all();
        
        $file = $request->file('image_url');
        $input['image_url'] = 'uploads/'.$file->getClientOriginalName();

        $file->move('uploads/',$input['image_url']);        


        $event = Event::create($input);
       /*
        $image_url = $input['image_url']->getClientOriginalName();
        $file -> move('uploads/',$image_url);
        $message = $input['image_url']->getClientOriginalName().' has been uploaded';*/ 


        return redirect()->back()/*->with('message', $message)*/;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $us = Auth::user();
         $event = Event::find($id);
        return view('admin.masterdata.eventdetail')
            ->with(['event'=>$event,
                'us'=>$us
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $input = $request->all();
        $event = event::find($id);
        $event->update($input);        

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=event::find($id);
        $event->delete();
        return redirect()->back();
    }
}
