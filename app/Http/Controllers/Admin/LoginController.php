<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;

class LoginController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */

    protected function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) 
        {                    
            Auth::attempt($credentials);            
            if(Auth::user()->role == "admin")
            {                
                $success = "true";            
                return redirect('adminpage/dashboard');
            }
            else
            {                
                $success = "false";
                return redirect()->action('eventController@adminlogin');
            }
            
        }
        else
        {            
            $success = "false";
            return redirect()->action('eventController@adminlogin');
        } 
    }

    protected function logout()
    {
        Auth::logout();

        return redirect()->action('eventController@adminlogin');
    }
}
