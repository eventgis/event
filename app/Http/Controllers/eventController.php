<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\User;
use App\Models\Category;
use Carbon\Carbon;
use Log;
use Input;
use File;
use Auth;


class eventController extends Controller
{
    

    public function index(){
        $us = Auth::user();
    	return view('home')->with(['us'=>$us]);
    }

    public function adminlogin(){
        return view('admin/auth/login');
    }


    public function newEvent(){
    	return view('new');
    }

    public function bookmarklist(){
        return view('bookmarklist');
    }

    public function attendlist(){
        return view('attendlist');
    }


    public function detail(){
    	return view('detail');
    }

    public function addevent(){

        $us = Auth::user();
        //$us = User::where('id', '=', Auth::user()->id);
        $user = User::all();
        $category = Category::all();
        return view('add')->with(['us'=>$us, 'category'=>$category, 'user'=>$user]);
    }

    public function create(Request $request)
    {
        $us = Auth::user();

        $input = $request->all();
        
        $file = $request->file('image_url');
        $input['image_url'] = 'uploads/'.$file->getClientOriginalName();

        $file->move('uploads/',$input['image_url']);        

        $event = Event::create($input);

        return redirect('detail/'.$event->id)->with(['us'=>$us, 'event' => $event]);
        
    }


    public function category(){
    	$us = Auth::user();
        return view('category')->with(['us'=>$us]);
    }


    public function login(){
    	return view('login');
    }

    public function register(){
    	return view('register');
    }

    public function about(){
    	$us = Auth::user();
        return view('about')->with(['us'=>$us]);
    }

    public function tesmap(){
        return view('tesmap');
    }

    public function todayEvents(){
        $today = Carbon::now();
        $event = Event::where('start_on', $today->toDateString())->get();
        
        foreach ($event as $e) {
            $e->is_done = 1;
            $e->save();
        }

        Log::info("Checking today finished events: ".$today->toDateString()."  ".$event->count());
    }
}
