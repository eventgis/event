﻿
#
# Source for table "event"
#

DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kategori_id` int(10) NOT NULL,
  `nama_event` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `waktu` date NOT NULL,
  `penyelenggara` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `harga` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan_event` text COLLATE utf8_unicode_ci NOT NULL,
  `poster` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "event"
#

INSERT INTO `event` VALUES (3,0,'testes1','Depok','','','2016-03-22','Lorem Ipsum','Free','habdhabfa dfa dc adhbvhdhafba dahfhadbfhadbf','','2016-03-14 19:43:14','2016-03-25 17:19:21'),(4,0,'tes edit','kajdhaud','','','2016-03-17','akjdah','jahdah','anhdjahdiufhancaudghaiuhfanh ajdhaiufhaiuhfiadhfadn haiudghfuiahdiufhadhfiadf ','','2016-03-25 17:43:56','2016-03-29 03:22:53');

#
# Source for table "kategori_event"
#

DROP TABLE IF EXISTS `kategori_event`;
CREATE TABLE `kategori_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "kategori_event"
#


#
# Source for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "migrations"
#


#
# Source for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `notelepon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `interested` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,'tiaraitalyana','tiara','tiaraitalyana@gmail.com','adaad','87893482798','Fall_for_You.jpg','','',NULL,'2016-03-29 07:55:02','2016-03-29 07:55:02'),(4,'tiara','tes','Nik0001@gmail.com','tiara23','adadfa','','','',NULL,'2016-03-29 09:18:59','2016-03-29 09:18:59');

